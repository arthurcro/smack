import UIKit

class AddChannelVC: UIViewController {
    @IBOutlet weak var channelNameTxt: UITextField!
    @IBOutlet weak var channelDescriptionTxt: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpview()
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func createChannelButtonTapped(_ sender: Any) {
        guard let channelName = channelNameTxt.text, channelNameTxt.text != "" else {return}
        guard let channelDesc = channelDescriptionTxt.text else {return}
        SocketService.instance.addChannel(name: channelName, description: channelDesc) { (success) in
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @objc func hideModal(_ recognizer : UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    func setUpview(){
        let closeTouch = UITapGestureRecognizer(target: bgView, action: #selector(AddChannelVC.hideModal(_:)))
        view.addGestureRecognizer(closeTouch)
        channelNameTxt.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceholder])
        channelDescriptionTxt.attributedPlaceholder = NSAttributedString(string: "Description", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceholder])
    }
}
