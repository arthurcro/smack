import UIKit

class ChannelVC: UIViewController, UITableViewDelegate,
UITableViewDataSource {
    
    
    @IBOutlet weak var userImage: CircleImg!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var channelTableView: UITableView!
    
    @IBAction func prepareForUnwind(segue : UIStoryboardSegue){}
    override func viewDidLoad() {
        super.viewDidLoad()
        //Rear view controller is shown
        channelTableView.delegate = self
        channelTableView.dataSource = self
        
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
        NotificationCenter.default.addObserver(self, selector: #selector(userDataDidChange), name: NOTIF_USER_DATA_DID_CHANGED, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.channelsLoaded(_:)), name: NOTIF_CHANNELS_LOADED, object: nil)
        
        SocketService.instance.getChannel { (success) in
            if success {
                self.channelTableView.reloadData()
            }
        }
        SocketService.instance.getMessage { (newMessage) in
            if newMessage.channelId != MessageService.instance.selectedChannel?.id && AuthService.instance.isLoggedIn {
                //A Channel has unread messages
                MessageService.instance.unreadChannel.append(newMessage.channelId)
                self.channelTableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpUserInfo()
    }
    @IBAction func loginButtonTapped(_ sender: Any) {
        if AuthService.instance.isLoggedIn {
            //Show profile page
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)
            
        }else{
            performSegue(withIdentifier: TO_LOGIN, sender: nil)
        }
    }
    @objc func userDataDidChange(_ notif : Notification){
      setUpUserInfo()
    }

    func setUpUserInfo(){
        if AuthService.instance.isLoggedIn {
            loginButton.setTitle(UserDataService.instance.name, for: .normal)
            userImage.image = UIImage(named : UserDataService.instance.avatarName)
            userImage.backgroundColor = UserDataService.instance.returnUIColor(avatarColor: UserDataService.instance.avatarColor)
        }else{
            loginButton.setTitle("Login", for: .normal)
            userImage.image = UIImage(named : "menuProfileIcon")
            userImage.backgroundColor = UIColor.clear
            self.channelTableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "channelCell", for : indexPath) as? ChannelCell {
            let channel = MessageService.instance.channels[indexPath.row]
            cell.configureCell(channel: channel)
            return cell;
        }
        return ChannelCell()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    @IBAction func addChannelButtonTapped(_ sender: Any) {
        if AuthService.instance.isLoggedIn {
            let addChannel = AddChannelVC()
            addChannel.modalPresentationStyle = .custom
            present(addChannel, animated: true, completion: nil)
        }
    }
    
    @objc func channelsLoaded(_ notif : Notification){
        channelTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = MessageService.instance.channels[indexPath.row]
        MessageService.instance.selectedChannel = channel
        NotificationCenter.default.post(name: NOTIF_CHANNEL_SELECTED, object: nil)
        self.revealViewController().revealToggle(animated: true)
        if MessageService.instance.unreadChannel.count > 0 {
            MessageService.instance.unreadChannel = MessageService.instance.unreadChannel.filter{$0 != channel.id}
        }
        let index = IndexPath(row: indexPath.row, section: 0)
        self.channelTableView.reloadRows(at: [index], with: .none)
        channelTableView.selectRow(at: index, animated: false, scrollPosition: .none)
    }
    
}

