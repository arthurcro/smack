import UIKit
import SocketIO

class SocketService: NSObject {
    static let instance = SocketService()
    override init() {
        super.init()
    }
    var socket : SocketIOClient = SocketIOClient(socketURL: URL(string: URL_BASE)!)
    
    func establishConnection(){
        socket.connect()
    }
    func closeConnection(){
        socket.disconnect()
    }
    func addChannel(name : String, description: String, completion : @escaping CompletionHandeler){
        socket.emit("newChannel", name, description)
        completion(true)
    }
    
    func getChannel(completion : @escaping CompletionHandeler){
        socket.on("channelCreated") { (dataArray, ack) in
            guard let channelName = dataArray[0] as? String else {return}
            guard let channelDesc = dataArray[1] as? String else {return}
            guard let channelId = dataArray[2] as? String else {return}
            MessageService.instance.channels.append(Channel(title: channelName, description: channelDesc, id: channelId))
            completion(true)
        }
    }
    func addMessage(messageBody : String, userId : String, channelId : String, completion : @escaping CompletionHandeler){
        let user = UserDataService.instance
        socket.emit("newMessage", messageBody, userId, channelId, user.name, user.avatarName, user.avatarColor)
        completion(true)
    }
    
    func getMessage(completion: @escaping (_ newMessage : Message) ->Void){
        socket.on("messageCreated") { (dataArray, ack) in
            
            guard let msgBody = dataArray[0] as? String else {return}
            guard let channelID = dataArray[2] as? String else {return}
            guard let userName = dataArray[3] as? String else {return}
            guard let userAvatar = dataArray[4] as? String else {return}
            guard let userAvatarColor = dataArray[5] as? String else {return}
            guard let msgID = dataArray[6] as? String else {return}
            guard let timeStamp = dataArray[7] as? String else {return}
             let newMessage = Message(message: msgBody, userName: userName, channelId: channelID, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: msgID, timeStamp: timeStamp)
            completion(newMessage)
        }
    }
    func getTypingUsers(_ completionHandler : @escaping (_ typingUsers : [String : String ])->Void){
        socket.on("userTypingUpdate") { (dataArray, ack) in
            guard let typingUsers = dataArray[0] as? [String : String] else {return}
            completionHandler(typingUsers)
        }
    }
}
