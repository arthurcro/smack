import Foundation

class UserDataService {
    static let instance = UserDataService()
    
    public private(set) var id = ""
    public private(set) var avatarColor = ""
    public private(set) var avatarName = ""
    public private(set) var email = ""
    public private(set) var name = ""
    
    func setUserData(id: String, avatarColor : String, avatarName : String, email : String, name : String){
        self.id = id
        self.avatarColor = avatarColor
        self.avatarName = avatarName
        self.email = email
        self.name = name
    }
    
    func setAvatarName(avatarName : String){
        self.avatarName = avatarName
    }
    
    func returnUIColor(avatarColor : String) -> UIColor{
        
        let scanner = Scanner(string: avatarColor)
        let skipped = CharacterSet(charactersIn: "[], ")
        let coma = CharacterSet(charactersIn : ",")
        let defaultColor = UIColor.lightGray
        var r,g,b,a : NSString?
        
        scanner.charactersToBeSkipped = skipped
        scanner.scanUpToCharacters(from: coma, into: &r)
        scanner.scanUpToCharacters(from: coma, into: &g)
        scanner.scanUpToCharacters(from: coma, into: &b)
        scanner.scanUpToCharacters(from: coma, into: &a)
        
        guard let rUnwrapped = r else {
            return defaultColor
        }
        guard let gUnwrapped = g else {
            return defaultColor
        }
        guard let bUnwrapped = b else {
            return defaultColor
        }
        guard let aUnwrapped = a else {
            return defaultColor
        }
        
        let rFloat = CGFloat(rUnwrapped.doubleValue)
        let gFloat = CGFloat(gUnwrapped.doubleValue)
        let bFloat = CGFloat(bUnwrapped.doubleValue)
        let aFloat = CGFloat(aUnwrapped.doubleValue)
        
        return UIColor(displayP3Red: rFloat, green: gFloat, blue: bFloat, alpha: aFloat)
    }
    func logoutUser(){
        id = ""
        avatarName = ""
        avatarColor = ""
        email = ""
        name = ""
        AuthService.instance.authToken = ""
        AuthService.instance.isLoggedIn = false
        AuthService.instance.userEmail = ""
        MessageService.instance.clearChannels()
        MessageService.instance.clearMessages()
    }
}
