import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        UserDataService.instance.logoutUser()
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGED, object: nil)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @objc func hideModal(_ recognizer : UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    func setUpView(){
        let tap = UITapGestureRecognizer(target: backgroundView, action: #selector(ProfileVC.hideModal(_:)))
        view.addGestureRecognizer(tap)
        userName.text = UserDataService.instance.name
        userEmail.text = UserDataService.instance.email
        userImage.image = UIImage(named : UserDataService.instance.avatarName)
        userImage.backgroundColor = UserDataService.instance.returnUIColor(avatarColor: UserDataService.instance.avatarColor)
    }
}
