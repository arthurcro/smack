import Foundation

struct Channel {
    
    public private(set) var title : String?
    public private(set) var description : String?
    public private(set) var id : String?
    
}
