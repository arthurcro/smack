import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var channelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1966308594)
        }else{
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    func configureCell(channel : Channel){
        let channelTitle = channel.title ?? ""
        channelName.text = "#\(channelTitle)"
        channelName.font = UIFont(name: "Avenir-Mediium", size: 17)
        
        for id in MessageService.instance.channels {
            if id.id == channel.id {
                channelName.font = UIFont(name: "Avenir-Heavy", size: 20)
            }
        }
    }
}
