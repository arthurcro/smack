//
//  MessageCell.swift
//  Smack
//
//  Created by Arthur Crocquevieille on 23/10/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    //outlets
    
    @IBOutlet weak var userImage: CircleImg!
    
    @IBOutlet weak var messageBodyLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(message : Message){
        messageBodyLabel.text = message.message
        userNameLabel.text = message.userName
        userImage.image = UIImage(named: message.userAvatar)
        userImage.layer.backgroundColor = UserDataService.instance.returnUIColor(avatarColor: message.userAvatarColor).cgColor
        
    }
    

}
