import Foundation

//Segues
let TO_LOGIN = "toLogin"
let TO_CREATE_ACCOUNT = "toCreateAccount"
let UNWIND = "unwind_channel"
let TO_PICK_AVATAR = "toPickAvatar"

//User defaults
let LOGGED_IN_KEY = "loggedin"
let TOKEN_KEY = "token"
let USER_EMAIL = "useremail"


//Completion handler
typealias CompletionHandeler = (_ Success : Bool )->()

//Urls
let URL_BASE = "http://localhost:3005/v1/"
let URL_REGISTER = "\(URL_BASE)account/register"
let URL_LOGIN = "\(URL_BASE)account/login"
let URL_CREATE_USER = "\(URL_BASE)user/add"
let URL_FIND_USER_BY_EMAIL = "\(URL_BASE)user/byEmail/"
let URL_CHANNELS = "\(URL_BASE)channel"
let URL_GET_MESSAGES = "\(URL_BASE)message/byChannel"


//Headers
let HEADER = ["content-type":"application/json; charset=utf-8"]
let BEARER_HEADER = ["Authorization":"Bearer \(AuthService.instance.authToken)",
    "content-type":"application/json"]

//Colors
let smackPurplePlaceholder = #colorLiteral(red: 0.2395215631, green: 0.3320434093, blue: 0.7513638139, alpha: 0.5)


//Notifications
let NOTIF_USER_DATA_DID_CHANGED = Notification.Name("UserNotifDataChanged")
let NOTIF_CHANNELS_LOADED = Notification.Name("ChannelsLoaded")
let NOTIF_CHANNEL_SELECTED = Notification.Name("ChannelSelected")

