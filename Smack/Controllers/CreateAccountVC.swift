import UIKit

class CreateAccountVC: UIViewController {

    //Outlets
    @IBOutlet weak var usernameTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    //Default Variables
    var avatarName = "profileDefault"
    var avatarColor = "[0.5,0.5,0.5,1]"
    var bgColor : UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserDataService.instance.avatarName != "" {
            userImg.image = UIImage(named : UserDataService.instance.avatarName)
            avatarName = UserDataService.instance.avatarName
            if avatarName.contains("light") && bgColor == nil{
                bgColor = UIColor.lightGray
            }
        }
    }
    @IBAction func closeCreateAccountTapped(_ sender: Any) {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }
    
    @IBAction func createAccountTapped(_ sender: Any) {
        spinnerView.isHidden = false
        spinnerView.startAnimating()
        guard let email = emailTxtField.text, emailTxtField.text != "" else {return}
        guard let pass = passwordTxtField.text, passwordTxtField.text != "" else {return}
        guard let name = usernameTxtField.text, usernameTxtField.text != "" else {return}
        
        AuthService.instance.registerUser(email: email, password: pass) { (success) in
            if success {
                AuthService.instance.logInUser(email: email, password: pass, completion: { (success) in
                    if success {
                        AuthService.instance.createUser(name: name, email: email, avatarName: self.avatarName, avatarColor: self.avatarColor, completion: { (success) in
                            if success {
                                self.spinnerView.isHidden = true
                                self.spinnerView.stopAnimating()
                                //On dit aux autres qu'on a created un user
                                NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGED, object: nil)
                                self.performSegue(withIdentifier: UNWIND, sender: nil)
                                
                            }
                        })
                    }
                })
            }
            
        }
        
    }
    
    @IBAction func chooseAvatarTapped(_ sender: Any) {
        performSegue(withIdentifier: TO_PICK_AVATAR, sender: nil)
    }
    
    @IBAction func generateBackgroundTapped(_ sender: Any) {
        let r = CGFloat(arc4random_uniform(255))/255
        let g = CGFloat(arc4random_uniform(255))/255
        let b = CGFloat(arc4random_uniform(255))/255
        let alpha = CGFloat(1)
        bgColor = UIColor(displayP3Red: r, green: g, blue: b, alpha: alpha)
        UIView.animate(withDuration: 0.2) {
            self.userImg.backgroundColor = self.bgColor
        }
        avatarColor = "[\(r),\(g),\(b),\(alpha)]"
    }
    @objc func handleTap(){
        view.endEditing(true)
    }
    func setUpView(){
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(CreateAccountVC.handleTap))
        view.addGestureRecognizer(tap)
        spinnerView.isHidden = true
        usernameTxtField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceholder])
        emailTxtField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceholder])
        passwordTxtField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceholder])
    }
    
}
