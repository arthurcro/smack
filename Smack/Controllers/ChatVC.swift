import UIKit
class ChatVC: UIViewController, UITableViewDelegate,
UITableViewDataSource {

    //Outlets
    @IBOutlet weak var smackBurgerButton: UIButton!
    @IBOutlet weak var channelNameLabel: UILabel!
    @IBOutlet weak var messageTxt: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var typingUsersLabel: UILabel!
    //Variables
    var isTyping = false
    
    override func viewDidLoad() {
        
        sendButton.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
        super.viewDidLoad()
        view.bindToKeyboard()
        let tap = UITapGestureRecognizer(target: self, action: #selector(ChatVC.handleTap))
        //Burger fonctionalité pour front reveal
        smackBurgerButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.userDataDidChanged), name: NOTIF_USER_DATA_DID_CHANGED, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.channelChanged(_:)), name: NOTIF_CHANNEL_SELECTED, object: nil)
        if AuthService.instance.isLoggedIn {
            AuthService.instance.findUserByEmail(completion: { (success) in
                if success {
                    NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGED, object: nil)
                }
            })
        }
        SocketService.instance.getMessage { (newMessage) in
            if newMessage.channelId == MessageService.instance.selectedChannel?.id && AuthService.instance.isLoggedIn {
                MessageService.instance.messages.append(newMessage)
                self.tableView.reloadData()
                if MessageService.instance.messages.count > 0 {
                    let indexPath = IndexPath(row: MessageService.instance.messages.count - 1 , section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom , animated: false)
                }
            }
        }
        SocketService.instance.getTypingUsers { (typingUsers) in
            guard let channelID = MessageService.instance.selectedChannel?.id else {return}
            var names = ""
            var numberOfTypers = 0
            for (typer, channel) in typingUsers {
                if typer != UserDataService.instance.name && channel == channelID {
                    if names == "" {
                        names = typer
                    }else{
                        names = "\(names), \(typer)"
                    }
                    numberOfTypers+=1
                }
            }
            if numberOfTypers > 0 && AuthService.instance.isLoggedIn {
                var verb = "is"
                if numberOfTypers > 1 {
                    verb = "are"
                }
                self.typingUsersLabel.text = "\(names) \(verb) typing a message.."
            }else{
                self.typingUsersLabel.text = ""
            }
        }
    }
    @objc func handleTap(){
        view.endEditing(true)
    }
    @objc func channelChanged(_ notif : Notification){
        updateWithChannel()
    }
    func updateWithChannel(){
        let channelName = MessageService.instance.selectedChannel?.title ?? ""
        channelNameLabel.text = "#\(channelName)"
        getMessages()
    }
    @objc func userDataDidChanged(_ notif : Notification){
        if AuthService.instance.isLoggedIn {
            onLogInGetMessages()
        }else{
            channelNameLabel.text = "Please Log In"
            tableView.reloadData()
        }
    }
    
    func onLogInGetMessages(){
        MessageService.instance.findAllChannels { (success) in
            if success {
                if MessageService.instance.channels.count > 0 {
                    MessageService.instance.selectedChannel = MessageService.instance.channels[0]
                    self.updateWithChannel()
                }else{
                    self.channelNameLabel.text = "No channels yet"
                }
            }
        }
    }
    
    @IBAction func textFieldTapped(_ sender: Any) {
        guard let channelID = MessageService.instance.selectedChannel?.id else {return}
        if messageTxt.text == "" {
            isTyping = false
            sendButton.isHidden = true
            SocketService.instance.socket.emit("stopType", UserDataService.instance.name, channelID)

        }else{
            if isTyping == false {
                SocketService.instance.socket.emit("startType", UserDataService.instance.name, channelID)
                sendButton.isHidden = false
                isTyping = true
            }
        }
    }
    @IBAction func sendMessageTapped(_ sender: Any) {
        if AuthService.instance.isLoggedIn {
            guard let channelId = MessageService.instance.selectedChannel?.id else {return}
            guard let message = messageTxt.text else {return}
            SocketService.instance.addMessage(messageBody: message, userId: UserDataService.instance.id, channelId: channelId, completion: { (success) in
                if success {
                    self.messageTxt.text = ""
                    self.messageTxt.resignFirstResponder()
                    SocketService.instance.socket.emit("stopType", UserDataService.instance.name,channelId)
                }else{
                    
                }
            })
        }
    }
    
    func getMessages(){
        guard let channedId = MessageService.instance.selectedChannel?.id else {return}
        MessageService.instance.findAllMessages(forChannel: channedId) { (success) in
            if success {
                self.tableView.reloadData()
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as? MessageCell{
            let message = MessageService.instance.messages[indexPath.row]
            cell.configureCell(message: message)
            return cell
        }
        return UITableViewCell()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.messages.count
    }
}
