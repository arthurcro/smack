import Foundation
import SwiftyJSON
import Alamofire

class MessageService {
    static let instance = MessageService()
    
    var channels = [Channel]()
    var messages = [Message]()
    var unreadChannel = [String]()
    var selectedChannel : Channel?

    func findAllChannels(completion : @escaping CompletionHandeler){
        Alamofire.request(URL_CHANNELS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error != nil {
                guard let data = response.data else { return }
                if let json = JSON(data: data).array {
                    for element in json {
                        
                        let name = element["name"].stringValue
                        let channelDescrp = element["description"].stringValue
                        let id = element["_id"].stringValue
                        let channel = Channel(title: name, description: channelDescrp, id: id)
                        self.channels.append(channel)
                        NotificationCenter.default.post(name: NOTIF_CHANNELS_LOADED, object: nil)
                        completion(true)
                    }
                }
            }else{
                debugPrint(response.result.error as Any!)
                completion(false)
            }
        }
    }
    func findAllMessages(forChannel channelID : String, completion : @escaping CompletionHandeler){
        Alamofire.request("\(URL_GET_MESSAGES)\(channelID)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                self.clearMessages()
                guard let data = response.data else {return}
                if let json = JSON(data: data).array {
                    for item in json {
                        let messageBody = item["messageBody"].stringValue
                        let channelID = item["channelID"].stringValue
                        let userName = item["userName"].stringValue
                        let userAvatar = item["userAvatar"].stringValue
                        let userAvatarColor = item["userAvatarColor"].stringValue
                        let id = item["_id"].stringValue
                        let timeStamp = item["timeStamp"].stringValue

                        let message = Message(message: messageBody, userName: userName, channelId: channelID, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: id, timeStamp: timeStamp)
                        self.messages.append(message)
                    }
                    completion(true)
                }
            }else{
                completion(false)
            }
        }
    }
    
    func clearMessages(){
        messages.removeAll()
    }
    func clearChannels(){
        channels.removeAll()
    }
}
