import UIKit

enum AvatarType {
    case dark
    case light
}
class AvatarPickerCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    
    func configureCell(index : Int, type: AvatarType){
        if type == AvatarType.dark {
            self.layer.backgroundColor = UIColor.lightGray.cgColor
            avatarImage.image = UIImage(named : "dark\(index)")
        }else{
            self.layer.backgroundColor = UIColor.darkGray.cgColor
            avatarImage.image = UIImage(named : "light\(index)")
        }
    }
    func setUpView(){
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
}
